/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
import * as obvm from './vm.mjs'
class OBDebugger {
    constructor(options) {
        this.options = options || {};
        this.instructionCount = 0;
    }
    static pausableInstructionWrap(instruction) {
        return function (st) {
            if (st.fsm.VM.pausing) {
                st.fsm.VM.pausing = false;
                throw new obvm.VMPausedException();
            }
            return instruction.apply(null, arguments)
        };
    }
    instructionWrap(instruction) {
        if (this.options.instructionWrap) {
            instruction = this.options.instructionWrap(instruction);
        }
        let that = this;
        return function () {
            that.instructionCount++;
            // console.log(that.instructionCount);
            return instruction.apply(null, arguments);
        };
    }
    registerWrap(register) {
        if (this.options.registerWrap) {
            register = this.options.registerWrap(register);
        }
        let that = this;
        return function () {
            that.instructionCount++;
            let ret = register.apply(null, arguments);
            return ret;
        };
    }
}

export {
    OBDebugger,
}
